<div align="center">
  <img src="https://gitlab.com/Linaro/tuxsuite/raw/master/tuxsuite_logo.png" alt="TuxSuite Logo" width="50%" />
</div>

[![Pipeline Status](https://gitlab.com/Linaro/tuxsuite/badges/master/pipeline.svg)](https://gitlab.com/Linaro/tuxsuite/pipelines)
[![coverage report](https://gitlab.com/Linaro/tuxsuite/badges/master/coverage.svg)](https://gitlab.com/Linaro/tuxsuite/commits/master)
[![PyPI version](https://badge.fury.io/py/tuxsuite.svg)](https://pypi.org/project/tuxsuite/)
[![Docker Pulls](https://img.shields.io/docker/pulls/tuxsuite/tuxsuite.svg)](https://hub.docker.com/r/tuxsuite/tuxsuite)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![PyPI - License](https://img.shields.io/pypi/l/tuxsuite)](https://gitlab.com/Linaro/tuxsuite/blob/master/LICENSE)

[Documentation](https://docs.tuxsuite.com/) - [Repository](https://gitlab.com/Linaro/tuxsuite) - [Issues](https://gitlab.com/Linaro/tuxsuite/-/issues)

[TuxSuite](https://tuxsuite.com), by Linaro, is a suite of tools and services
to help with Linux kernel development. The TuxSuite CLI (this repo) is the
supported interface to TuxBuild and TuxTest.

![tuxsuite screencast](https://people.linaro.org/~dan.rue/tuxbuild/demo.gif
"tuxsuite screencast")

To request access, email us at tuxsuite@linaro.org or fill out our [access
request form](https://forms.gle/3NaW5fuNykGstsMq6).

[[_TOC_]]

# Introduction

The TuxSuite CLI supports two services: TuxBuild and TuxTest.

## TuxBuild

<img src="docs/tuxbuild_icon.svg" alt="TuxBuild Logo" width="100px" align="left">

TuxBuild is an on demand API for building massive quantities of Linux kernels
in parallel. It is used at scale in production by
[LKFT](https://lkft.linaro.org/) and
[ClangBuiltLinux](https://clangbuiltlinux.github.io/) as well as many
individual Linux kernel engineers.

TuxBuild is accessed by running `tuxsuite build` and `tuxsuite build-set`.

## TuxTest

<img src="docs/tuxtest_icon.svg" alt="TuxTest Logo" width="100px" align="left">

TuxTest is an on demand API for testing Linux kernels reliably and quickly. It
is currently in Alpha phase and will soon be available to TuxBuild users.

## Install and Configure

### Install using pip

TuxSuite requires Python version 3.6 or greater, and is available using pip.

To install tuxsuite on your system globally:

```
sudo pip3 install -U tuxsuite
```

To install tuxsuite to your home directory at ~/.local/bin:

```
pip3 install -U --user tuxsuite
```

To upgrade tuxsuite to the latest version, run the same command you ran to
install it.

### Install using docker

tuxsuite is also available as a docker container at
[tuxsuite/tuxsuite](https://hub.docker.com/r/tuxsuite/tuxsuite).

For example, to run tuxsuite via docker:

```
docker run tuxsuite/tuxsuite tuxsuite build --help
```

### Setup Config

The Authentication token needs to be stored in `~/.config/tuxsuite/config.ini`.
The minimal format of the ini file is given below:

```
$ cat ~/.config/tuxsuite/config.ini
[default]
token=vXXXXXXXYYYYYYYYYZZZZZZZZZZZZZZZZZZZg
```

Alternatively, the `TUXSUITE_TOKEN` environment variable may be provided.

If you do not have a tuxsuite token, please reach out to us at
tuxsuite@linaro.org.

## Examples

### tuxsuite build

Submit a build request using the tuxsuite command line interface. This will
wait for the build to complete before returning by default.

```
tuxsuite build --git-repo 'https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git' --git-ref master --target-arch arm64 --kconfig defconfig --toolchain gcc-9
```

### tuxsuite build-set

Create a tuxsuite config file with a basic set of build combinations defined.

```
cat <<EOF > basic.yaml
sets:
  - name: basic
    builds:
      - {target_arch: arm,    toolchain: gcc-9,    kconfig: multi_v7_defconfig}
      - {target_arch: arm,    toolchain: gcc-10,   kconfig: multi_v7_defconfig}
      - {target_arch: arm,    toolchain: clang-10, kconfig: multi_v7_defconfig}
      - {target_arch: arm64,  toolchain: gcc-9,    kconfig: defconfig}
      - {target_arch: arm64,  toolchain: gcc-10,   kconfig: defconfig}
      - {target_arch: arm64,  toolchain: clang-10, kconfig: defconfig}
      - {target_arch: i386,   toolchain: gcc-9,    kconfig: defconfig}
      - {target_arch: i386,   toolchain: gcc-10,   kconfig: defconfig}
      - {target_arch: x86_64, toolchain: gcc-9,    kconfig: defconfig}
      - {target_arch: x86_64, toolchain: gcc-10,   kconfig: defconfig}
      - {target_arch: x86_64, toolchain: clang-10, kconfig: defconfig}
      - {target_arch: arc,    toolchain: gcc-10,   kconfig: hsdk_defconfig}
      - {target_arch: riscv,  toolchain: gcc-10,   kconfig: defconfig}
      - {target_arch: mips,   toolchain: gcc-10,   kconfig: ci20_defconfig}
EOF
# Build the build set defined in the config file named 'basic.yaml'
tuxsuite build-set --git-repo 'https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git' --git-ref master --tux-config basic.yaml --set-name basic
```

All the parameters can be specified in the build-set itself and invoke tuxsuite "tuxsuite build-set  --tux-config \<basic\>.yaml --set-name \<set-name\>"

## Projects and Developers using tuxsuite

- [LKFT](https://lkft.linaro.org/) - Linaro's Linux Kernel Functional Testing
  uses tuxsuite with
  [gitlab-ci](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)
  to continuously build upstream Linux kernels. The kernels are then
  functionally tested on a variety of hardware using
  [LAVA](https://www.lavasoftware.org/).
- [ClangBuiltLinux](https://clangbuiltlinux.github.io/) uses TuxBuild to
  validate hundreds of combinations of Linux kernels and LLVM environments.
- Lee Jones uses a [GitLab CI
  pipeline](https://gitlab.com/Linaro/lkft/users/lee.jones/lag-linaro-linux/-/pipelines)
  to validate his 3.18 kernel maintainership. The gitlab pipeline, tuxsuite
  config, and README.md documenting its setup are defined in the
  [kernel-pipeline](https://gitlab.com/Linaro/lkft/users/lee.jones/kernel-pipeline)
  repository.

## Community and Support

The TuxSuite team may be engaged through chat, email, or gitlab issues.

To chat with us, [join our public Discord](https://discord.gg/4hhTzUrj5M), or
our IRC channels #tuxsuite and #tuxmake on
[freenode.net](https://freenode.net/).

Questions, comments or feedback are always welcome by private email at
tuxsuite@linaro.org.

Finally, gitlab issues are used to track bugs and feature requests in both
[tuxsuite](https://gitlab.com/Linaro/tuxsuite/-/issues) and
[tuxmake](https://gitlab.com/Linaro/tuxmake/-/issues) projects.
